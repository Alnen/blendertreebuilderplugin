from collections import namedtuple
import ply.yacc as yacc
from src.rpe import Item, ItemType
from src.lex import get_lexer

tokens = (
    'VARIABLE_NAME',
    'CONSTANT',
    'PREDICATE_OP',
    'RELATION_OP',
    'ADDITIVE_OP',
    'MULTIPLICATIVE_OP',
    'ARROW',
    'BRACKET_OPEN',
    'BRACKET_CLOSE'
)

AST = namedtuple('AST', ['constant_table', 'rule_table'])
RuleDescription = namedtuple('RuleDescription', ['header', 'condition', 'probability', 'substitution'])
Header = namedtuple('Header', ['name', 'arguments'])
SubstitutionAtom = namedtuple('SubstitutionAtom', ['name', 'parameters'])
Variable = namedtuple('Variable', ['name', 'expr'])

def get_parser(lexer, start = 'PROGRAM'):
    constant_table = []
    rule_table = []
    out_table_name = ''
    if start == 'PROGRAM':
        start = 'program_parsed'
        out_table_name = 'program_parsetab'
    elif start == 'RULE':
        start = 'rule_declaration'
        out_table_name = 'rule_parsetab'
    elif start == 'VARIABLE':
        start = 'variable_declaration'
        out_table_name = 'variable_parsetab'
    else:
        raise Exception

    def p_program_parsed(p):
        """
            program_parsed : program
        """
        nonlocal rule_table
        nonlocal constant_table
        p[0] = AST(constant_table, rule_table)
        constant_table = []
        rule_table = []

    def p_rule_parsed(p):
        """
            program : rule_declaration ';' program
        """
        # TODO VALIDATE
        rule_table.insert(0, p[1])

    def p_variable_parsed(p):
        """
            program : variable_declaration ';' program
        """
        # TODO VALIDATE
        constant_table.insert(0, p[1])

    def p_programm_end(p):
        """
            program :
        """
        pass

    def p_rule_description(p):
        """
            rule_declaration : header condition probability ARROW substitution
        """
        p[0] = RuleDescription(p[1], p[2], p[3], p[5])

    def p_header(p):
        """
            header : VARIABLE_NAME arguments
        """
        p[0] = Header(p[1], p[2])

    def p_arguments(p):
        """
            arguments : '(' argument_pack ')'
        """
        p[0] = p[2]

    def p_arguments_empty(p):
        """
            arguments :
        """
        p[0] = []

    def p_argument_pack(p):
        """
            argument_pack : VARIABLE_NAME additional_arguments
        """
        p[0] = []
        p[0].append(p[1])
        p[0].extend(p[2])

    def p_additional_arguments(p):
        """
            additional_arguments : ',' VARIABLE_NAME additional_arguments
        """
        p[0] = []
        p[0].append(p[2])
        p[0].extend(p[3])

    def p_additional_arguments_empty(p):
        """
            additional_arguments :
        """
        p[0] = []

    def p_condition(p):
        """
            condition : '?' expr
        """
        p[0] = p[2]


    def p_condition_empty(p):
        """
            condition :
        """
        p[0] = [Item(True, ItemType.CONSTANT)]

    def p_expr(p):
        """
            expr : SWITCH_LEXER_TO_EXPRESSION predicate_expr SWITCH_LEXER_FROM_EXPRESSION
        """
        p[0] = p[2]

    def p_SWITCH_LEXER_TO_EXPRESSION(p):
        """
            SWITCH_LEXER_TO_EXPRESSION :
        """
        p.lexer.push_state('EXPRESSION')


    def p_SWITCH_LEXER_FROM_EXPRESSION(p):
        """
            SWITCH_LEXER_FROM_EXPRESSION :
        """
        p.lexer.pop_state()

    def p_predicate_expr(p):
        """
            predicate_expr : predicate_expr PREDICATE_OP relation_expr
        """
        p[0] = p[1]
        p[0].extend(p[3])
        p[0].append(Item(p[2], ItemType.BINARY_OPERATION))

    def p_predicate_expr_pass(p):
        """
            predicate_expr : relation_expr
        """
        p[0] = p[1]

    def p_relation_expr(p):
        """
            relation_expr : relation_expr RELATION_OP additive_expr
        """
        p[0] = p[1]
        p[0].extend(p[3])
        p[0].append(Item(p[2], ItemType.BINARY_OPERATION))

    def p_relation_expr_pass(p):
        """
            relation_expr : additive_expr
        """
        p[0] = p[1]

    def p_additive_expr(p):
        """
            additive_expr : additive_expr ADDITIVE_OP multiplicative_expr
        """
        p[0] = p[1]
        p[0].extend(p[3])
        p[0].append(Item(p[2], ItemType.BINARY_OPERATION))

    def p_additive_expr_pass(p):
        """
            additive_expr : multiplicative_expr
        """
        p[0] = p[1]

    def p_multiplicative_expr(p):
        """
            multiplicative_expr : multiplicative_expr MULTIPLICATIVE_OP unary_expr
        """
        p[0] = p[1]
        p[0].extend(p[3])
        p[0].append(Item(p[2], ItemType.BINARY_OPERATION))

    def p_multiplicative_expr_pass(p):
        """
            multiplicative_expr : unary_expr
        """
        p[0] = p[1]

    def p_unary_expr(p):
        """
            unary_expr : unary_operator bracket_expr
        """
        p[0] = p[2]
        p[0].append(Item(p[1], ItemType.UNARY_OPERATION))

    def p_unary_operator_pass(p):
        """
            unary_expr : bracket_expr
        """
        p[0] = p[1]

    def p_unary_operator(p):
        """
            unary_operator : '!'
                           | ADDITIVE_OP
        """
        p[0] = p[1]

    def p_bracket_expr_pass(p):
        """
            bracket_expr : '(' predicate_expr ')'
        """
        p[0] = p[2]

    def p_bracket_expr_variable(p):
        """
            bracket_expr : VARIABLE_NAME
        """
        p[0] = []
        p[0].append(Item(p[1], ItemType.VARIABLE))

    def p_bracket_expr_constant(p):
        """
            bracket_expr : CONSTANT
        """
        p[0] = []
        p[0].append(Item(float(p[1]), ItemType.CONSTANT))

    def p_probability(p):
        """
            probability : ':' SWITCH_LEXER_TO_PROBABILITY BRACKET_OPEN CONSTANT BRACKET_CLOSE SWITCH_LEXER_FROM_PROBABILITY
        """
        p[0] = p[4]


    def p_probability_pass(p):
        """
            probability :
        """
        p[0] = 1

    def p_SWITCH_LEXER_TO_PROBABILITY(p):
        """
            SWITCH_LEXER_TO_PROBABILITY :
        """
        p.lexer.push_state('PROBABILITY')


    def p_SWITCH_LEXER_FROM_PROBABILITY(p):
        """
            SWITCH_LEXER_FROM_PROBABILITY :
        """
        p.lexer.pop_state()

    def p_substitution(p):
        """
            substitution : item substitution
        """
        p[0] = []
        p[0].append(p[1])
        p[0].extend(p[2])

    def p_substitution_pass(p):
        """
            substitution :
        """
        p[0] = []

    def p_item(p):
        """
            item : VARIABLE_NAME parameters
        """
        p[0] = SubstitutionAtom(p[1], p[2])

    def p_parametrs(p):
        """
            parameters : '('  parameter_pack ')'
        """
        p[0] = p[2]

    def p_parametrs_pass(p):
        """
            parameters :
        """
        p[0] = []

    def p_parametr_pack(p):
        """
            parameter_pack : expr additional_parameters
        """
        p[0] = []
        p[0].append(p[1])
        p[0].extend(p[2])

    def p_parametr_pack_pass(p):
        """
            parameter_pack :
        """
        p[0] = []

    def p_additional_parameters(p):
        """
            additional_parameters : ',' expr additional_parameters
        """
        p[0] = []
        p[0].append(p[2])
        p[0].extend(p[3])

    def p_additional_parameters_pass(p):
        """
            additional_parameters :
        """
        p[0] = []

    def p_variable_declaration(p):
        """
            variable_declaration : VARIABLE_NAME '=' expr
        """
        p[0] = Variable(p[1], tuple(p[3]))

    # Error rule for syntax errors
    def p_error(p):
        print("Syntax error in input!", p)

    # Build the parser
    parser = yacc.yacc(tabmodule=out_table_name)
    return parser


if __name__ == "__main__":
    def test_rules():
        print('=================TEST_RULE================')
        import src.lex as lexer
        from src.yacc import get_parser
        from src.rpe import evaluate_reverse_polish
        axiom = 'F X'
        lexer = lexer.get_lexer()
        #parser = get_parser(lexer, start='PROGRAM')
        parser = get_parser(lexer, start='RULE')
        axiom = parser.parse("S->" + axiom)
        rule1 = parser.parse("X->X + Y F", lexer=lexer)
        rule2 = parser.parse("Y -> F X - Y", lexer=lexer)
        out = parser.parse("A(s, t)? t<10 -> F( 1+s, (s+t ) /2) [ F + ^ ^ F ] f", lexer=lexer)
        data1 = [{'s': 9, 't': 5}]
        data2 = [{'s': 10, 't': 11}]
        print(evaluate_reverse_polish(out.condition, data1))
        print(evaluate_reverse_polish(out.substitution[0].parameters[0], data1))
        print(evaluate_reverse_polish(out.substitution[0].parameters[1], data1))
        print(evaluate_reverse_polish(out.condition, data2))
        print(evaluate_reverse_polish(out.substitution[0].parameters[0], data2))
        print(evaluate_reverse_polish(out.substitution[0].parameters[1], data2))

    def test_variable():
        print('=================TEST_VARIABLE================')
        import src.lex as lexer
        from src.yacc import get_parser
        from src.rpe import evaluate_reverse_polish
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='VARIABLE')
        rule1 = parser.parse("X = 1", lexer=lexer)
        rule2 = parser.parse("Y = X + (X + 2)/2", lexer=lexer)
        print(rule1.name, '=',evaluate_reverse_polish(rule1.expr, [{}, {}]))
        print(rule2.name, '=',evaluate_reverse_polish(rule2.expr, [{}, {'X': 5}]))


    def test_program():
        print('=================TEST_PROGRAM================')
        import src.lex as lexer
        from src.yacc import get_parser
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='PROGRAM')
        axiom = 'F X'
        #VARIABLES
        program = ''
        var1 = "X = 1"
        var2 = "Y = X + (X + 2)/2"
        axiom = "S->"+axiom
        rule1 = "X->X + Y F"
        rule2 = "Y -> F X - Y"
        program = [var1, var2, axiom, rule1, rule2]
        program_text = ''
        for item in program:
            program_text += item+';'
        import logging
        logging.basicConfig(
            level = logging.DEBUG,
            filename = "parselog.txt",
            filemode = "w",
            format = "%(filename)10s:%(lineno)4d:%(message)s"
        )
        log = logging.getLogger()
        out = parser.parse(program_text, lexer=lexer, debug=log)
        print('DONE')
    test_rules()
    test_variable()
    test_program()