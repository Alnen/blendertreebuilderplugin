import ply.lex as lex




def get_lexer():
    states = (
        ('EXPRESSION', 'exclusive'),
        ('PROBABILITY', 'exclusive')
    )

    tokens = (
        'VARIABLE_NAME',
        'CONSTANT',
        'PREDICATE_OP',
        'RELATION_OP',
        'ADDITIVE_OP',
        'MULTIPLICATIVE_OP',
        'ARROW',
        'BRACKET_OPEN',
        'BRACKET_CLOSE'
    )

    literals = ':(),!=;?'

    #  INITIAL
    t_ANY_ignore = " \t"

    def t_ANY_newline(t):
        r'\n+'
        t.lexer.lineno += t.value.count("\n")

    def t_ANY_error(t):
        print("Illegal character '%s'" % t.value[0], t.lexer.current_state())
        t.lexer.skip(1)

    def t_ANY_ARROW(t):
        r'->'
        return t

    t_ANY_CONSTANT = r'(\d)+(\.\d*)?'

    #  INITIAL
    t_VARIABLE_NAME =  r'\+|-|&|\^|\||/|\\|\[|\]|([a-zA-Z][a-zA-Z0-9_]*)'

    #  EXPRESSION
    t_EXPRESSION_VARIABLE_NAME = r'[a-zA-Z][a-zA-Z0-9_]*'
    t_EXPRESSION_PREDICATE_OP = r'(\&\&)|(\|\|)'
    t_EXPRESSION_RELATION_OP = r'>|<|(>=)|(<=)|(==)|(!=)'
    t_EXPRESSION_ADDITIVE_OP = r'\+|-'
    t_EXPRESSION_MULTIPLICATIVE_OP = r'\*|/'

    #  PROBABILITY
    t_PROBABILITY_BRACKET_OPEN = r'\['
    t_PROBABILITY_BRACKET_CLOSE = r'\]'

    return lex.lex()

if __name__ == "__main__":
    pass
