from collections import namedtuple
from src.rpe import evaluate_reverse_polish

RuleKey = namedtuple('RuleKey', ['name', 'argc'])
RuleValue = namedtuple('RuleValue', ['arguments', 'condition', 'probability', 'substitution'])
VariableDeclaration = namedtuple('VariableDeclaration', ['name', 'expr'])
RuleWithParams = namedtuple('RuleWithParams', ['key', 'parameter_list'])


def _process_rule_description(rule_description):
    rule_key = RuleKey(
        rule_description.header.name,
        len(rule_description.header.arguments)
    )
    rule_value = RuleValue(
        rule_description.header.arguments,
        rule_description.condition,
        rule_description.probability,
        rule_description.substitution
    )
    return rule_key, rule_value


def _evaluate_variables_table(variable_list):
    answer = {}
    scopes = [answer]

    suspeended_evaluations = []
    unprocessed_resolved = []
    for variable in variable_list:
        try:
            value = evaluate_reverse_polish(variable.expr, scopes)
            answer[variable.name] = value
            unprocessed_resolved.insert(0, variable.name)
        except LookupError as e:
            suspeended_evaluations.append((variable, e.args[0]))
        else:
            while len(unprocessed_resolved) > 0:
                resolved = unprocessed_resolved.pop(0)
                j = 0
                while j < len(suspeended_evaluations):
                    var, unresolved = suspeended_evaluations[j]
                    if resolved in unresolved:
                        unresolved.remove(resolved)
                        if len(unresolved) is 0:
                            value = evaluate_reverse_polish(var.expr, scopes)
                            answer[var.name] = value
                            unprocessed_resolved.append(var.name)
                            suspeended_evaluations.pop(j)
                            j -= 1
                    j += 1
    if len(suspeended_evaluations) is not 0:
        raise ValueError([var.name for var in suspeended_evaluations])
    return answer


def _get_line_traverser(line, scopes_list):
    for item in line:
        parameters = []
        for parameter in item.parameters:
            try:
                value = evaluate_reverse_polish(parameter, scopes_list)
                parameters.append(value)
            except LookupError as e:
                print('No value for variables:')
                for variable_name in e.args[0]:
                    print('-',variable_name)
                raise e
        yield RuleWithParams(RuleKey(item.name, len(parameters)), parameters)
    else:
        raise StopIteration


def resolve_propabilitie_issue(possible_rule_list):
    return possible_rule_list[0]


class _NoRuleFound(Exception):
    pass


def tree_walker(AST):
    # Fill rule_table
    rule_table = {}
    for rule_desc in AST.rule_table:
        rule_key, rule_value = _process_rule_description(rule_desc)
        try:
            rule_table[rule_key].append(rule_value)
        except KeyError:
            rule_table[rule_key] = [rule_value]
    # Evaluate variable_table
    global_variables = _evaluate_variables_table(AST.constant_table)

    class tree_walker:
        def __init__(self, rule_table, global_variables, n):
            self.rule_table = rule_table
            self.global_variables = global_variables
            self.propability_handler = resolve_propabilitie_issue
            self.max_n = n
            #
            self.stack = []
            current_rule = self.rule_table[('S', 0)][0]
            self.stack.append(iter(_get_line_traverser(current_rule.substitution, [self.rule_table])))
            self.n = 0

        def __iter__(self):
            return self

        def __next__(self):
            stack = self.stack
            self.n
            while stack:
                # try to produce new tree list TODO move to end
                rule_with_params = None
                while stack:
                    try:
                        list_generator = stack[self.n]
                        rule_with_params = next(list_generator)
                    except StopIteration:
                        stack.pop()
                        self.n -= 1
                    else:
                        break
                if not stack:
                    raise StopIteration
                if self.n == self.max_n:
                    # last level. make action
                    return rule_with_params
                else:
                    try:
                        # intermediate level. generate next level
                        # get list of possible rule_values
                        rule_value_list = self.rule_table.get(rule_with_params.key, None)
                        if not rule_value_list:
                            raise _NoRuleFound
                        # filter rules with false condition
                        rules_passed_condition_check = []
                        for possible_rule in rule_value_list:
                            rules_local_scope = dict(zip(possible_rule.arguments, rule_with_params.parameter_list))
                            if evaluate_reverse_polish(possible_rule.condition, [rules_local_scope, self.global_variables]):
                                rules_passed_condition_check.append(possible_rule)
                        if not rules_passed_condition_check:
                            raise _NoRuleFound
                        # resolve filter rules with false condition
                        current_rule_value = self.propability_handler(rules_passed_condition_check)
                        if not current_rule_value:
                            raise _NoRuleFound
                        # process current value
                        rules_local_scope = dict(zip(possible_rule.arguments, rule_with_params.parameter_list))
                        stack.append(iter(_get_line_traverser(current_rule_value.substitution, [rules_local_scope, self.global_variables])))
                        self.n += 1
                    except _NoRuleFound:
                        # There is no substitution so make pretend that n is nmax
                        return rule_with_params
    return tree_walker(rule_table, global_variables, 3)


if __name__ == '__main__':
    def test1():
        import src.lex as lexer
        from src.yacc import get_parser
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='VARIABLE')
        rule1 = parser.parse("X = 1", lexer=lexer)
        rule2 = parser.parse("Y = X + (X + 2)/2", lexer=lexer)
        rule3 = parser.parse("Z = Y + X", lexer=lexer)
        print(rule1.name, '=',evaluate_reverse_polish(rule1.expr, [{}, {}]))
        print(rule2.name, '=',evaluate_reverse_polish(rule2.expr, [{}, {'X': 5}]))
        variable_table = _evaluate_variables_table([rule3, rule2, rule1])
        for name, value in variable_table.items():
            print(name, '=', value)

    def test2():
        import src.lex as lexer
        from src.yacc import get_parser
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='PROGRAM')
        axiom = 'F X'
        #VARIABLES
        program = ''
        var1 = "X = 1"
        var2 = "Z = Y + X"
        var3 = "Y = X + (X + 2)/2"
        axiom = "S->"+axiom
        rule1 = "X->X + Y F"
        rule2 = "Y -> F X - Y"
        program = [var1, var2, var3, axiom, rule1, rule2]
        program_text = ''
        for item in program:
            program_text += item+';'
        out = parser.parse(program_text, lexer=lexer)
        tree_walker(out)
        print('DONE')


    def test3():
        import src.lex as lexer
        from src.yacc import get_parser
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='PROGRAM')

        #0 F X
        #1 F X Y
        #2 F X Y f F
        #3 F X Y f F f F

        #VARIABLES
        program = ''
        var1 = "X = 1"
        var2 = "Z = Y + X"
        var3 = "Y = X + (X + 2)/2"
        axiom = 'F X'
        rule1 = "X->X Y "
        rule2 = "Y -> f F"
        #
        axiom = "S->"+axiom
        program = [var1, var2, var3, axiom, rule1, rule2]
        program_text = ''
        for item in program:
            program_text += item+';'
        out = parser.parse(program_text, lexer=lexer)
        action_handlers = {('F', 0): lambda: print('F'), ('f', 0): lambda: print('f')}
        it = tree_walker(out)
        for item in it:
            handler = action_handlers.get(item.key, None)
            if handler:
                handler(*item.parameter_list)
            else:
                print('No handler for: (', item.key.name, ':', item.key.argc, '):', item.parameter_list)
        print('DONE')

    def test4():
        import src.lex as lexer
        from src.yacc import get_parser
        lexer = lexer.get_lexer()
        parser = get_parser(lexer, start='PROGRAM')

        #0 F X
        #1 F X Y
        #2 F X Y F F
        #3 F X Y f F F F

        #VARIABLES
        program = ''
        var1 = "X = 2"
        var2 = "Z = Y + X"
        var3 = "Y = X + (X + 2)/2"
        axiom = 'F X(1)'
        rule1 = "X(n)->X(n+1) Y(n+1)"
        rule2 = "Y(n)? n!=X -> f(n) F"
        rule3 = "Y(n)? n==X -> f(n) f(n)"
        #
        axiom = "S->"+axiom
        program = [var1, var2, var3, axiom, rule1, rule2, rule3]
        program_text = ''
        for item in program:
            program_text += item+';'
        out = parser.parse(program_text, lexer=lexer)
        action_handlers = {('F', 0): lambda: print('F'), ('f', 1): lambda x: print('f:', x)}
        it = tree_walker(out)
        for item in it:
            handler = action_handlers.get(item.key, None)
            if handler:
                handler(*item.parameter_list)
            else:
                print('No handler for: (', item.key.name, ':', item.key.argc, '):', item.parameter_list)
        print('DONE')

    test4()



