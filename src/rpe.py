from enum import Enum
from collections import namedtuple

class ItemType(Enum):
    VARIABLE = 0
    CONSTANT = 1
    UNARY_OPERATION = 2
    BINARY_OPERATION = 3
Item = namedtuple('Item', ['value', 'type'])

def _get_unresolved_dependencies_list(command_sequence, scopes_list, answer):
    for item in command_sequence:
        if item.type == ItemType.VARIABLE:
            for scope in scopes_list:
                if item.value in scope:
                    break
            else:
                if item.value not in answer:
                    answer.append(item.value)

def evaluate_reverse_polish(commands: [Item], scopes_list=[]):
    assert len(commands) > 0
    stack = []
    command_sequence = iter(commands)
    for item in command_sequence:
        if item.type is ItemType.CONSTANT:
            stack.append(item.value)
        elif item.type is ItemType.VARIABLE:
            for scope in scopes_list:
                try:
                    value = scope[item.value]
                    break
                except KeyError:
                    continue
            else:
                # find other dependencies
                unresolved_dependencies = [item.value]
                _get_unresolved_dependencies_list(command_sequence, scopes_list, unresolved_dependencies)
                raise LookupError(unresolved_dependencies)
            stack.append(value)
        elif item.type is ItemType.UNARY_OPERATION:
            op = item.value
            value = stack.pop()
            if op == '+':
                stack.append(value)
            elif op == '!':
                stack.append(not value)
            elif op == '-':
                stack.append(-value)
            else:
                # ERROR
                pass
        elif item.type is ItemType.BINARY_OPERATION:
            op = item.value
            value2 = stack.pop()
            value1 = stack.pop()
            # LOGIC_OPERATORS
            if op == '&&':
                stack.append(bool(value1 and value2))
            elif op == '||':
                stack.append(bool(value1 or value2))
            # COMPARISON_OPERATORS
            elif op == '>':
                stack.append(bool(value1 > value2))
            elif op == '>=':
                stack.append(bool(value1 >= value2))
            elif op == '<':
                stack.append(bool(value1 < value2))
            elif op == '<=':
                stack.append(bool(value1 <= value2))
            elif op == '==':
                stack.append(bool(value1 == value2))
            elif op == '!=':
                stack.append(bool(value1 != value2))
            # ADDITIVE_OPERATORS
            elif op == '+':
                stack.append(value1 + value2)
            elif op == '-':
                stack.append(value1 - value2)
            # MULTIPLICATIVE_OPERATORS
            elif op == '*':
                stack.append(value1 * value2)
            elif op == '/':
                stack.append(value1 / value2)
            elif op == '%':
                stack.append(value1 % value2)
            else:
                # ERROR
                pass
    assert len(stack) is 1
    return stack.pop()

if __name__ == '__main__':
    scopes = [{'a': 10, 'b': 5, 'c': 3}]
    # 2*2 + 3*3
    commands_1 = [
        Item(2, ItemType.CONSTANT),
        Item(2, ItemType.CONSTANT),
        Item('*', ItemType.BINARY_OPERATION),
        Item(3, ItemType.CONSTANT),
        Item(3, ItemType.CONSTANT),
        Item('*', ItemType.BINARY_OPERATION),
        Item('+', ItemType.BINARY_OPERATION)
    ]
    print(evaluate_reverse_polish(commands_1))
    # a>5 && b<6
    commands_2 = [
        Item('a', ItemType.VARIABLE),
        Item(5, ItemType.CONSTANT),
        Item('>', ItemType.BINARY_OPERATION),
        Item('b', ItemType.VARIABLE),
        Item(6, ItemType.CONSTANT),
        Item('<', ItemType.BINARY_OPERATION),
        Item('&&', ItemType.BINARY_OPERATION)
    ]
    print(evaluate_reverse_polish(commands_2, scopes))
